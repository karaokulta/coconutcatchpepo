﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadObjBehaviour : MonoBehaviour
{
    public int lessPoints = -50;
    public float speed;
    public Rigidbody2D rb;
    private void Start()
    {
        lessPoints += GameManager.instance.minusPoints;
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (!GameManager.instance.onTime)//Desaparecerán cuando el timer llegue a 0
        {
            this.gameObject.SetActive(false);
        }

        
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(0, speed);
    }
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
           
            GameManager.instance.LessScore();
            
            this.gameObject.SetActive(false);
        }

        if (collision.gameObject.CompareTag("Out"))
        {
            this.gameObject.SetActive(false);
        }
    }

   
}
