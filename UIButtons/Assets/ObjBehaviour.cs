﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjBehaviour : MonoBehaviour
{
    public float speed;
    public Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        if (!GameManager.instance.onTime)//Desaparecerán cuando el timer llegue a 0
        {
            this.gameObject.SetActive(false);
        }
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(0, speed);
    }
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (this.gameObject.CompareTag("Object"))
            {
                GameManager.instance.AddScore(100);//Agregamos el score desde aquí
                this.gameObject.SetActive(false);
            }

            

        }

        if (collision.CompareTag("Out"))
        {
            
            this.gameObject.SetActive(false);
        }

    }
}
