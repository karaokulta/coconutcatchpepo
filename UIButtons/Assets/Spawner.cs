﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject objToSpawn;
    public float newPosition;
    public float firstRetard, spawnRate;
    public float secondRetard;//Estas dos nos servirán si ponemos muchos spawners para que tarden en spawnear
   
    
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnObject", spawnRate + firstRetard, spawnRate + secondRetard);//2.1f,2f
      
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.onTime)
        {

            if (GameManager.instance.ready)
            {
                newPosition = Random.Range(-1.8f, 1.8f);
                StartCoroutine(Position());
            }
            
        } else//Solo funcionará si el timer sigue corriendo
        {
            StopAllCoroutines();
            CancelInvoke("SpawnObject");
        }

        
        
    }

    IEnumerator Position()
    {
        yield return new WaitForSeconds(0.5f);
        transform.position = new Vector2(newPosition, 7.5f);
    }

    void SpawnObject()
    {
        Instantiate(objToSpawn, transform.position, transform.rotation);
    }

    
}
