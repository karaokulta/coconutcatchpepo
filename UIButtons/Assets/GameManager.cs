﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{


    public static GameManager instance;

    public float timer = 60;
   
    public int score;
    public Text showScore;
    public Text showTime, readyText, timeUp, finalScore, x2, textRecovered;
    public bool onTime = true;//Si es falso, el juego acaba
    public bool ready = false;
    public bool doublePoints, isPaused, wasHit;
    public Animator animator;
    public int minusPoints;
    public Animator plus5, recovered;
    public GameObject reset, pauseButton, continueButton;
    private void Awake()
    {
        if (instance == null)
        {

            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this);
        }

        showScore.text = "Score: " + score;
        StartCoroutine("readyToPlay");
        
        animator.GetComponent<Animator>();
        
    }

    private void Start()
    {
        InvokeRepeating("evenLessPoints", 10f, 10f);
        plus5.GetComponent<Animator>();
        recovered.GetComponent<Animator>();
        
    }

    private void Update()
    {
        if (onTime && ready) { 
        timer -= Time.deltaTime;
        showTime.text = "Time Left: " + (timer).ToString("0");
            

        }

        if (timer <= 0)
        {
            timer = 0;
          
            onTime = false;
            pauseButton.SetActive(false);
            timeUp.gameObject.SetActive(true);
            finalScore.gameObject.SetActive(true);
            finalScore.text = " Your score: " + score;
            reset.SetActive(true);
            

            
        }

        if (doublePoints)
        {
            StartCoroutine(deactivateDoublePoints());
        }

        if (wasHit)
        {
            StartCoroutine(DisablePenalty());
        }

        
    }

    // Update is called once per frame
    public void AddScore(int scoreAdd)
    {
        if(!doublePoints)
        
        {
            score += scoreAdd;
            showScore.text = "Score: " + score;
        } else
        {
            score += scoreAdd * 2;
            showScore.text = "Score: " + score;
        }
    }

    public void RecoverPoints()
    {
        if (wasHit)
        {
            score += minusPoints;
            showScore.text = "Score: " + score;
            recovered.gameObject.SetActive(true);
            textRecovered.text = "+ " + minusPoints;
            recovered.SetTrigger("Appear");
        } else
        {
            score += 150;
            showScore.text = "Score: " + score;
            recovered.gameObject.SetActive(true);
            textRecovered.text = "+ 150";
            recovered.SetTrigger("Appear");
        }
       
    }

    public void LessScore()
    {
        score -= minusPoints;
        showScore.text = "Score: " + score;
        
    }

    public void AddTime(float timeAdd)
    {
        timer += 5f;
        showTime.text = "Time Left: " + (timer).ToString("0");
        plus5.SetTrigger("Appear");
        
       
    }

    IEnumerator readyToPlay()
    {
        
        readyText.text = "Are";
        yield return new WaitForSeconds(0.5f);
        readyText.text = "You";
        yield return new WaitForSeconds(0.5f);
        readyText.text = "Ready?";
        yield return new WaitForSeconds(0.9f);
        ready = true;
        readyText.text = " GO!";
        yield return new WaitForSeconds(0.8f);
        readyText.gameObject.SetActive(false);
        pauseButton.SetActive(true);

        
    }

    IEnumerator DisablePenalty()
    {
        yield return new WaitForSeconds(3f);
        wasHit = false;
    }

  

    public void PlayAnimation()
    {
        animator.Play("Damage");
        Handheld.Vibrate();
    }

    public void evenLessPoints()
    {
        minusPoints += 10;
    }

    IEnumerator deactivateDoublePoints()
    {
        x2.gameObject.SetActive(true);
        showScore.color = new Color(255, 0, 176);
        yield return new WaitForSeconds(5f);
        doublePoints = false;
        showScore.color = new Color(255, 255, 255);
        x2.gameObject.SetActive(false);

    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
        pauseButton.SetActive(false);
        continueButton.SetActive(true);
        isPaused = true;
    }

    public void ContinueGame()
    {
        Time.timeScale = 1f;
        pauseButton.SetActive(true);
        continueButton.SetActive(false);
        isPaused = false;
    }
}
