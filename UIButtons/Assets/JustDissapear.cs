﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JustDissapear : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
       InvokeRepeating("Dissapear", 2f, 2f);
    }

    // Update is called once per frame
    void Dissapear()
    {
       
        this.gameObject.SetActive(false);
    }
}
