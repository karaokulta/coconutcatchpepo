﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class BonusBehaviour : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed;
    public float lateralMove;
    public Rigidbody2D rb;
    public int movement;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        switch (movement)
        {
            case 1:

                StartCoroutine(caseOneMovement());
                break;

            case 2:
                StartCoroutine(caseTwoMovement());
                break;

            case 3:
                StartCoroutine(caseThreeMovement());
                break;

            default:
                Debug.Log("Error");
                break;
        }
    }
    private void Update()
    {
        if (!GameManager.instance.onTime)//Desaparecerán cuando el timer llegue a 0
        {
            this.gameObject.SetActive(false);
        }

    }

    private void FixedUpdate()
    {
     



    }
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {


            if (this.gameObject.CompareTag("TimeBonus"))
            {
                GameManager.instance.AddTime(5f);//Agregamos el score desde aquí
                this.gameObject.SetActive(false);
            }

            if (this.gameObject.CompareTag("Double"))
            {
                GameManager.instance.doublePoints = true;
                this.gameObject.SetActive(false);
            }

            if (this.gameObject.CompareTag("Recover"))
            {
                GameManager.instance.RecoverPoints();
                this.gameObject.SetActive(false);
            }

        }

        if (collision.CompareTag("Out"))
        {

            this.gameObject.SetActive(false);
        }

       

    }

    IEnumerator caseOneMovement()
    {
        rb.velocity = new Vector2(0, speed);
        yield return new WaitForSeconds(1f);
        rb.velocity = new Vector2(lateralMove, speed);
        yield return new WaitForSeconds(1f);
        lateralMove *= -1;
        rb.velocity = new Vector2(lateralMove, speed);
        yield return new WaitForSeconds(1f);
        lateralMove *= -1;
        rb.velocity = new Vector2(lateralMove, speed);
        yield return new WaitForSeconds(1f);
        lateralMove *= -1;
        rb.velocity = new Vector2(lateralMove, speed);
    }

    IEnumerator caseTwoMovement()
    {
        rb.velocity = new Vector2(0, speed);
        yield return new WaitForSeconds(1.7f);
        speed *= -1;
        rb.velocity = new Vector2(lateralMove, speed);
        yield return new WaitForSeconds(0.5f);
        speed *= -1;
        lateralMove *= -1;
        rb.velocity = new Vector2(lateralMove, speed);
        yield return new WaitForSeconds(1f);
        lateralMove *= -1;
        rb.velocity = new Vector2(lateralMove, speed);
    }

    IEnumerator caseThreeMovement()
    {
        rb.velocity = new Vector2(0, speed);
        yield return new WaitForSeconds(1.5f);
        rb.velocity = new Vector2(lateralMove, 0);
        yield return new WaitForSeconds(1.0f);
        lateralMove *= -1;
        rb.velocity = new Vector2(lateralMove, 0);
        yield return new WaitForSeconds(1f);
        rb.velocity = new Vector2(0, speed);
    }
}
